# eZ Platform AlloyEditor Custom Tag Link

This bundle is a temporary wrapper for [EZP-31772: implement custom tag link](https://github.com/ezsystems/ezplatform-richtext/pull/157) pull request.

## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ezplatform-alloyeditor-custom-tag-link
    ```

2. Clear browser caches and enjoy!

![demo UI](doc/demo-ui.png)
